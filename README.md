# Cordova iOS Youtube Video Plugin

This plugin plays youtube videos.

## version
0.0.1

## Using

Install the plugin

    $ cordova plugin add https://jagraj47@bitbucket.org/jagraj47/ios-youtube-corvoda-plugin.git --save

Use following code in your component where playing videos

```js
    declare const iOSYoutubePlayer; // In case you are getting typescript errorMessage
    // Play video
    iOSYoutubePlayer.openVideo('youtube-video-id');
```